No | Use Case | Deskripsi | Score
---|---|---|---
1 | Menjelajahi Map | Player mengekplorasi map yang ada. | 10
2 | Menyerang Musuh | Player dapat menyerang musuh yang tersedia. | 10
3 | Menerima Serangan dari musuh | Selain dapat menyerang Player juga dapat di serang oleh musuh. | 9
4 | Memiliki nyawa | Player mempunyai nyawa yang dimana akan menghilang saat menerima serangan dari musuh, Saat nyawa sudah habis maka player akan meninggoy. | 9
5 | Melakukan Pertarungan | Player dapat bertarung dengan musuh yang tersedia yang dimana musuh sudah di lengkapi dengan kecerdasan yaitu dapat mengejar player. | 10
6 | Bermain Bersama Pemain Lain | Pengguna dapat bermain bersama dengan pemain lain dalam pertarungan melawan musuh dalam dunia fantasy. | 9
7 | Melihat coin | Saat sudah mengalahkan musuh akan mendapatkan 20 coin. | 7
8 | Menghancurkan alam | Menyerang dadaunan juga termasuk merusak alam. | 7
9 | Mengikuti Event | Pengguna dapat mengikuti event khusus yang diselenggarakan dalam permainan. | 8
10 | Berinteraksi dengan NPC | Pengguna dapat berinteraksi dengan karakter non-playable (NPC) dalam permainan untuk mendapatkan informasi, tugas, atau hadiah. | 9
11 | Menjelajahi Dunia Terbuka | Pengguna dapat menjelajahi dunia terbuka dalam permainan untuk menemukan lokasi baru, sumber daya, atau fitur tersembunyi. | 10
12 | Membangun dan Mengelola Rumah | Pengguna dapat membangun dan mengelola rumah pribadi dalam permainan dengan mengatur dekorasi, furniture, dan fitur lainnya. | 8
13 | Mengumpulkan Barang Langka | Pengguna dapat mencari dan mengumpulkan barang-barang langka atau koleksi dalam permainan. | 9
14 | Berpartisipasi dalam Pertandingan PvP | Pengguna dapat berpartisipasi dalam pertandingan PvP (Player vs Player) untuk menguji kemampuan dan strategi mereka melawan pemain lain. | 10
15 | Membuka Akses ke Area Tersembunyi | Pengguna dapat menemukan dan membuka akses ke area tersembunyi dalam permainan dengan menyelesaikan tantangan atau memenuhi persyaratan tertentu. | 8
16 | Mengendarai Kendaraan | Pengguna dapat menggunakan kendaraan seperti mobil, pesawat terbang, atau hewan tunggangan dalam permainan. | 9
17 | Berdagang dan Berbisnis | Pengguna dapat melakukan aktivitas perdagangan, membuka toko, atau menjalankan bisnis dalam permainan. | 7
18 | Menyelesaikan Puzzle dan Tantangan | Pengguna dapat menyelesaikan berbagai puzzle atau tantangan logika yang ada dalam permainan. | 8
19 | Mengembangkan Keahlian Karakter | Pengguna dapat mengembangkan keahlian dan kemampuan karakter mereka melalui latihan, pelatihan, atau pengalaman. | 9
20 | Memperoleh Prestasi dan Gelar | Pengguna dapat memperoleh prestasi dan gelar khusus berdasarkan pencapaian mereka dalam permainan. | 7

### NO 2
### Koneksi Ke database
```php
$host = "localhost";
$username = "Byhq~";
$password = "qwerty";
$database = "genshin";

function create_connection() {
    global $host, $username, $password, $database;
    $conn = new mysqli($host, $username, $password, $database);
    if ($conn->connect_error) {
        die("Koneksi ke database gagal: " . $conn->connect_error);
    }
    return $conn;
}
```
### RESTful API
#### CREATE

```php 
$app->post('/players', function (Request $request, Response $response, array $args) {
        $name = $request->getParam('name');
        $level = $request->getParam('level');
        $score = $request->getParam('score');

        $conn = create_connection();
        $sql = "INSERT INTO players (name, level, score) VALUES (?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sii", $name, $level, $score);

    if ($stmt->execute()) {
        $data = [
            'message' => 'Data pemain berhasil ditambahkan'
        ];
        return $response->withJson($data, 200);
    } else {
        $data = [
            'message' => 'Error: ' . $stmt->error
        ];
        return $response->withJson($data, 500);
    }

    $stmt->close();
    $conn->close();
    }); 
```
    
Fungsi `post` pada kode tersebut adalah metode HTTP POST yang digunakan dalam framework atau library pengembangan web. Fungsi ini digunakan untuk menangani permintaan POST dari klien (browser atau aplikasi lainnya) ke server.

Ketika permintaan POST diterima oleh server, fungsi ini akan dieksekusi. Pada kode yang diberikan, fungsi `post` memiliki empat parameter yaitu `$request`, `$response`, `$args`, dan `array`.

1. `$request`: Parameter ini digunakan untuk mengakses data yang dikirimkan oleh klien dalam permintaan POST. Dalam kode ini, data yang dikirimkan melalui permintaan POST diambil menggunakan metode `$request->getParam('parameter_name')`. Misalnya, `$name = $request->getParam('name')` digunakan untuk mengambil nilai dari parameter 'name' yang dikirimkan oleh klien.

2. `$response`: Parameter ini digunakan untuk menghasilkan respons yang akan dikirimkan kembali ke klien. Dalam kode ini, fungsi `$response->withJson($data, $status)` digunakan untuk mengembalikan respons dalam format JSON dengan kode status yang ditentukan. `$data` adalah data yang akan dikirimkan kembali ke klien, misalnya 'message' yang berisi pesan berhasil atau pesan error.

3. `$args`: Parameter ini berisi argumen tambahan yang bisa digunakan dalam fungsi. Namun, dalam kode yang diberikan, parameter ini tidak digunakan.

4. `array`: Parameter ini juga merupakan argumen tambahan yang bisa digunakan dalam fungsi. Namun, dalam kode yang diberikan, parameter ini juga tidak digunakan.

Jadi, fungsi `post` pada kode tersebut bertugas untuk menangani permintaan POST yang dikirimkan oleh klien, mengambil data dari permintaan, melakukan operasi penyimpanan data ke database, dan mengirimkan respons kembali ke klien berdasarkan hasil eksekusi operasi tersebut.

#### READ 
```php
    $app->get('/players', function (Request $request, Response $response, array $args) {
        $conn = create_connection();
        $sql = "SELECT * FROM players";
        $result = $conn->query($sql);
        $players = [];

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $players[] = $row;
        }
        return $response->withJson($players, 200);
    } else {
        $data = [
            'message' => 'Data pemain tidak ditemukan'
        ];
        return $response->withJson($data, 404);
    }

    $conn->close();
    });
```

Fungsi `get` pada kode diatas adalah metode HTTP GET yang digunakan dalam framework atau library pengembangan web. 

Ketika permintaan GET diterima oleh server, fungsi ini akan dieksekusi. Pada kode yang diberikan, fungsi `get` memiliki tiga parameter yaitu `$request`, `$response`, dan `$args`.

1. `$request`: Parameter ini digunakan untuk mengakses data yang dikirimkan oleh klien dalam permintaan GET. Dalam kode ini, data yang dikirimkan melalui permintaan GET dapat diakses untuk pengambilan parameter atau query string.

2. `$response`: Parameter ini digunakan untuk menghasilkan respons yang akan dikirimkan kembali ke klien. Dalam kode ini, fungsi `$response->withJson($data, $status)` digunakan untuk mengembalikan respons dalam format JSON dengan kode status yang ditentukan. `$data` adalah data yang akan dikirimkan kembali ke klien, dalam hal ini, data pemain yang ditemukan dalam database.

3. `$args`: Parameter ini berisi argumen tambahan yang bisa digunakan dalam fungsi. Dalam kode yang diberikan, parameter ini tidak digunakan.

Fungsi `get` bertugas untuk menangani permintaan GET yang dikirimkan oleh klien. Fungsi ini melakukan koneksi ke database, melakukan operasi SELECT untuk mengambil data pemain dari tabel 'players', dan mengirimkan respons kembali ke klien dengan data pemain yang ditemukan (jika ada) atau pesan bahwa data pemain tidak ditemukan (jika tidak ada).


#### UPDATE 
```php
    $app->put('/players/{id}', function (Request $request, Response $response, array $args) {
        $player_id = $args['id'];
        $name = $request->getParam('name');
        $level = $request->getParam('level');
        $score = $request->getParam('score');

        $conn = create_connection();
        $sql = "UPDATE players SET name=?, level=?, score=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("siii", $name, $level, $score, $player_id);

    if ($stmt->execute()) {
        $data = [
            'message' => 'Data pemain berhasil diperbarui'
        ];
        return $response->withJson($data, 200);
    } else {
        $data = [
            'message' => 'Error: ' . $stmt->error
        ];
        return $response->withJson($data, 500);
    }

    $stmt->close();
    $conn->close();
    });
```

Fungsi `put` pada kode diatas adalah metode HTTP PUT yang digunakan dalam framework atau library pengembangan web. 

Ketika permintaan PUT diterima oleh server, fungsi ini akan dieksekusi. Pada kode yang diberikan, fungsi `put` memiliki tiga parameter yaitu `$request`, `$response`, dan `$args`.

1. `$request`: Parameter ini digunakan untuk mengakses data yang dikirimkan oleh klien dalam permintaan PUT. Dalam kode ini, data yang dikirimkan melalui permintaan PUT dapat diakses untuk pengambilan parameter seperti `name`, `level`, dan `score`.

2. `$response`: Parameter ini digunakan untuk menghasilkan respons yang akan dikirimkan kembali ke klien. Dalam kode ini, fungsi `$response->withJson($data, $status)` digunakan untuk mengembalikan respons dalam format JSON dengan kode status yang ditentukan. `$data` adalah data yang akan dikirimkan kembali ke klien, dalam hal ini, pesan bahwa data pemain berhasil diperbarui atau pesan error jika terjadi masalah dalam proses perbaruan.

3. `$args`: Parameter ini berisi argumen tambahan yang bisa digunakan dalam fungsi. Dalam kode yang diberikan, parameter ini digunakan untuk mengambil `id` pemain yang akan diperbarui dari URL.

Jadi, fungsi `put` pada kode tersebut bertugas untuk memperbarui data pemain berdasarkan permintaan PUT yang dikirimkan oleh klien dan mengirimkan respons kembali ke klien dengan pesan yang sesuai.

##### DELETE
``` php
    $app->delete('/weapon/{id}', function (Request $request, Response $response, array $args) {
        $player_id = $args['id'];

        $conn = create_connection();
        $sql = "DELETE FROM players WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $player_id);

    if ($stmt->execute()) {
        $data = [
            'message' => 'Data pemain berhasil dihapus'
        ];
        return $response->withJson($data, 200);
    } else {
        $data = [
            'message' => 'Error: ' . $stmt->error
        ];
        return $response->withJson($data, 500);
    }

    $stmt->close();
    $conn->close();
    });
```

Fungsi `delete` pada kode diatas adalah metode HTTP DELETE yang digunakan dalam framework atau library pengembangan web. 

Ketika permintaan DELETE diterima oleh server, fungsi ini akan dieksekusi. Pada kode yang diberikan, fungsi `delete` memiliki tiga parameter yaitu `$request`, `$response`, dan `$args`.

1. `$request`: Parameter ini digunakan untuk mengakses data yang dikirimkan oleh klien dalam permintaan DELETE. Dalam kode ini, data yang dikirimkan melalui permintaan DELETE tidak terlalu relevan karena hanya membutuhkan `id` pemain yang akan dihapus.

2. `$response`: Parameter ini digunakan untuk menghasilkan respons yang akan dikirimkan kembali ke klien. Dalam kode ini, fungsi `$response->withJson($data, $status)` digunakan untuk mengembalikan respons dalam format JSON dengan kode status yang ditentukan. `$data` adalah data yang akan dikirimkan kembali ke klien, dalam hal ini, pesan bahwa data pemain berhasil dihapus atau pesan error jika terjadi masalah dalam proses penghapusan.

3. `$args`: Parameter ini berisi argumen tambahan yang bisa digunakan dalam fungsi. Dalam kode yang diberikan, parameter ini digunakan untuk mengambil `id` pemain yang akan dihapus dari URL.

Fungsi `delete` pada kode tersebut bertugas untuk menghapus data pemain berdasarkan permintaan DELETE yang dikirimkan oleh klien dan mengirimkan respons kembali ke klien dengan pesan yang sesuai.


### SOAL 4

##### 1. Regex (Regular Expression):
Kita ingin memvalidasi apakah nama pemain yang dikirim melalui permintaan POST hanya terdiri dari huruf dan spasi. Kita dapat menggunakan built-in function preg_match dalam PHP dengan regex sebagai berikut:

```php
// ...
$name = $request->getParam('name');

// Validasi nama hanya terdiri dari huruf dan spasi
if (!preg_match('/^[a-zA-Z\s]+$/', $name)) {
    $data = [
        'message' => 'Nama pemain tidak valid. Nama harus terdiri dari huruf dan spasi.'
    ];
    return $response->withJson($data, 400);
}

// ...
```

##### 2. Substring:
Mengambil tiga karakter pertama dari nama pemain yang ada dalam database. Kita dapat menggunakan built-in function substr dalam PHP sebagai berikut:

```php
// ...
while ($row = $result->fetch_assoc()) {
    $row['name'] = substr($row['name'], 0, 3); // Mengambil tiga karakter pertama dari nama
    $players[] = $row;
}
// ...
```

##### 3. Built-in Function Lainnya (array_column di PHP):
Mengambil semua nilai level dari array players yang diperoleh dari database. Kita dapat menggunakan built-in function array_column dalam PHP sebagai berikut:

```php
// ...
while ($row = $result->fetch_assoc()) {
    $players[] = $row;
}

$levels = array_column($players, 'level'); // Mengambil semua nilai level dari array players

// ...
```

### SOAL 5
1. Menampilkan character yang memiliki bintang kelangkaan lebih dari 4 keatas atau lebih
```php 
// ...
$sql = "SELECT id_character, name_character, region, type_weapon, character_star, element_character
            FROM `character`
            WHERE type_weapon IN (
                SELECT type_weapon
                FROM `character`
                WHERE character_star >= 4
            )";
// ...
```

2. Mengambil karakter-karakter yang memiliki jumlah karakter bintang tertinggi:
```php
// ...
$sql = "SELECT id_character, name_character, region, type_weapon, character_star, element_character
FROM `character`
WHERE character_star = (
    SELECT MAX(character_star)
    FROM `character`
)";
// ...
```

3. Mengambil karakter-karakter yang memiliki jenis senjata unik:
```php
// ...
$sql = "SELECT id_character, name_character, region, type_weapon, character_star, element_character
FROM `character`
WHERE type_weapon IN (
    SELECT DISTINCT type_weapon
    FROM `character`
)";
// ...
```

4. Mengambil karakter-karakter dari wilayah tertentu yang memiliki karakter bintang tertinggi:
```php
// ...
$sql = "SELECT id_character, name_character, region, type_weapon, character_star, element_character
FROM `character`
WHERE region = 'Mondstadt' AND character_star = (
    SELECT MAX(character_star)
    FROM `character`
    WHERE region = 'Mondstadt'
)";
// ...
```

### 6 
Berikut adalah contoh penggunaan transaksi (transaction) pada kode PHP Anda:

```php
$app->post('/players', function (Request $request, Response $response, array $args) {
    // Memulai transaksi
    $conn = create_connection();
    $conn->begin_transaction();

    try {
        // Melakukan operasi insert data
        $name = $request->getParam('name');
        $level = $request->getParam('level');
        $score = $request->getParam('score');

        $sql = "INSERT INTO players (name, level, score) VALUES (?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sii", $name, $level, $score);

        if ($stmt->execute()) {
            // Jika operasi insert berhasil, commit transaksi
            $conn->commit();

            $data = [
                'message' => 'Data pemain berhasil ditambahkan'
            ];
            return $response->withJson($data, 200);
        } else {
            // Jika operasi insert gagal, rollback transaksi
            $conn->rollback();

            $data = [
                'message' => 'Error: ' . $stmt->error
            ];
            return $response->withJson($data, 500);
        }

        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        // Jika terjadi exception, rollback transaksi dan tangani error
        $conn->rollback();

        $data = [
            'message' => 'Error: ' . $e->getMessage()
        ];
        return $response->withJson($data, 500);
    }
});
```

Pada contoh di atas, transaksi dimulai dengan memanggil `$conn->begin_transaction()`. Setelah itu, operasi insert data dilakukan di dalam blok `try`. Jika operasi insert berhasil, transaksi di-commit menggunakan `$conn->commit()`. Jika terjadi error pada operasi insert atau terjadi exception, transaksi di-rollback menggunakan `$conn->rollback()`.

### 7

1. Procedure:
```php
$sql = "CREATE PROCEDURE calculate_total_score()
        BEGIN
            SELECT SUM(score) AS total_score FROM players;
        END";

$stmt = $conn->prepare($sql);
$stmt->execute();
$stmt->close();
```

Kode di atas untuk membuat sebuah procedure yang dapat dijalankan untuk menghitung total skor dari seluruh pemain.

2. Function:
```php
$sql = "CREATE FUNCTION calculate_average_score() RETURNS FLOAT
        BEGIN
            DECLARE average FLOAT;
            SELECT AVG(score) INTO average FROM players;
            RETURN average;
        END";

$stmt = $conn->prepare($sql);
$stmt->execute();
$stmt->close();
```

Kode di atas untuk membuat sebuah function yang dapat menghitung rata-rata skor dari seluruh pemain.

3. Trigger:
```php
$sql = "CREATE TRIGGER update_player_level
        BEFORE UPDATE ON players
        FOR EACH ROW
        BEGIN
            IF NEW.score >= 1000 THEN
                SET NEW.level = 'High';
            ELSEIF NEW.score >= 500 THEN
                SET NEW.level = 'Medium';
            ELSE
                SET NEW.level = 'Low';
            END IF;
        END";

$stmt = $conn->prepare($sql);
$stmt->execute();
$stmt->close();
```

Kode di atas untuk membuat sebuah trigger yang akan mengupdate level pemain berdasarkan skor yang diperbarui.

### 8 

1. GRANT:
```php
$sql = "GRANT SELECT ON game_database.players TO 'username'@'localhost'";

$stmt = $conn->prepare($sql);
$stmt->execute();
$stmt->close();
```
Kode di atas untuk memberikan izin akses SELECT pada tabel players kepada pengguna dengan username tertentu.

2. REVOKE:
```php
$sql = "REVOKE INSERT ON game_database.players FROM 'username'@'localhost'";

$stmt = $conn->prepare($sql);
$stmt->execute();
$stmt->close();
```
Kode di atas untuk mencabut izin akses INSERT pada tabel players dari pengguna dengan username tertentu.

### 9

1. Foreign Key Constraint:
```php
$sql_players = "CREATE TABLE players (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  team_id INT,
  FOREIGN KEY (team_id) REFERENCES teams(id)
)";
$conn->query($sql_players);
```
Kita membuat tabel "players" dengan kolom "team_id" sebagai foreign key yang terhubung ke kolom "id" pada tabel "teams". Hal ini memastikan bahwa setiap nilai dalam kolom "team_id" harus ada di kolom "id" dari tabel "teams".

2. Index Constraint:
```php
$sql_players = "CREATE TABLE players (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  score INT,
  INDEX idx_score (score)
)";
$conn->query($sql_players);
```
Kita membuat indeks pada kolom "score" di tabel "players" dengan menggunakan sintaks "INDEX". Ini memungkinkan pencarian dan pengurutan berdasarkan kolom "score" menjadi lebih efisien.

3. Unique Key Constraint:
```php
$sql_players = "CREATE TABLE players (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255) UNIQUE
)";
$conn->query($sql_players);
```
Kita membuat kolom "email" menjadi unique key di tabel "players". Ini memastikan bahwa setiap nilai dalam kolom "email" harus unik, sehingga tidak ada duplikasi email dalam tabel.
